import Link from "next/link";
import { useState } from "react";

const Header = () => {
  // state management
  const [isExpanded, toggleExpansion] = useState(false);

  return (
    <header className="flex items-center justify-between flex-wrap bg-white p-6 shadow">
      <div className="flex items-center flex-shrink-0 text-white mr-6">
        <Link href="/">
          <a>
            <img src="logo.png" className="fill-current h-6" />
          </a>
        </Link>
      </div>
      <div className="block lg:hidden">
        <button
          class="flex items-center px-3 py-2 border rounded  bg-gray-700 text-white border-gray-700 hover:text-grey-900 hover:border-gray-900"
          onClick={() => toggleExpansion(!isExpanded)}
        >
          <svg
            className="fill-current h-3 w-3"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>

      <div
        className={`${
          isExpanded ? `block` : `hidden`
        } w-full block flex-grow lg:flex lg:items-center lg:w-auto`}
      >
        <div className="text-sm lg:flex-grow"></div>
        <div>
          {[
            { title: "Home", route: "/" },
            { title: "About", route: "/about" },
          ].map((navigationItem) => (
            <>
              <Link href={navigationItem.route}>
                <a
                  href="#responsive-header"
                  className="block mt-4 lg:inline-block lg:mt-0 text-gray-700 hover:text-gray-900 mr-4"
                >
                  {navigationItem.title}
                </a>
              </Link>
            </>
          ))}
          <a
            href="#"
            className="inline-block text-sm px-4 py-2 leading-none border rounded text-gray-700 border-gray-700 hover:border-transparent hover:text-white hover:bg-gray-900 mt-4 lg:mt-0"
          >
            Signup
          </a>
        </div>
      </div>
    </header>
  );
};

export default Header;
